import shutil
from os.path import dirname, join

from packagetools.builder import *
from packagetools.command import cygwin_path, run

VERSION = "0.0.1"
RECIPE_DIR = dirname(__file__)


# this time, we inherit from PackageShortRecipe
class Recipe(PackageShortRecipe):
    def describe(self) -> PackageInfo:
        return PackageInfo(
            description="Hello World (but with bash script this time)",
            id="hello-world-using-bash",
            pretty_name="Hello World Using Bash",
            version=VERSION,
        )

    # this function replace all checkout, build, test, package functions
    def all_steps(self, out_dir: str):
        # if we execute "bash" directly, wsl is called instead.
        # using which allows to call msys2 binary
        bash = shutil.which("bash")
        # paths should be converted to cygwin world
        out_dir = cygwin_path(out_dir)
        recipe = cygwin_path(join(RECIPE_DIR, "recipe.sh"))
        run(bash, recipe, out_dir)


PackageBuilder(Recipe()).make()
