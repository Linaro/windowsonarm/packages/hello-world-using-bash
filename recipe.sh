#!/usr/bin/env bash

set -euo pipefail
set -x

die()
{
    echo "$@" >&2
    exit 1
}

[ $# -eq 1 ] || die "usage: out_dir"
out_dir=$1; shift

checkout()
{
    git clone https://gitlab.com/Linaro/windowsonarm/packages/hello-world/
    cd hello-world/src
}

build()
{
    cmd /c "cl.exe src.cc /c /EHsc"
    cmd /c "link.exe src.obj /out:bin.exe"
}

test()
{
    ./bin.exe
}

package()
{
    mv bin.exe $out_dir
}

checkout
build
test
package
